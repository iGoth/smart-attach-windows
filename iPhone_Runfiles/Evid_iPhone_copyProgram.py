#!/usr/bin/env python


from docx import *
import time ,os,sys,datetime, string
import fnmatch,shutil

#Copy Images from one folder to other
def copy_images(working_dir,img_dir,img_names):
       rootDirs = img_names
       imgList = []
       alist_filter = ['jpg','bmp','png','gif','PNG']
       for r,d,f in os.walk(img_dir):
              for file in f:
                     changed_file = file.replace(" ","")
                     if file[-3:]in alist_filter:
                            sourceFolder = os.path.join(img_dir, file)
                            print "done"
                            try:
                                   shutil.copy(sourceFolder,working_dir)
                            except OSError:
                                   pass
                            os.rename(file,changed_file)
                            imgList.append(changed_file)
       return imgList

#Delete images from Temp folder
def del_temp_media(working_dir):
       mediafolder = "%s/template/word/media" %working_dir
       for r, d, f in os.walk(mediafolder):
              for name in f:
                     os.remove(os.path.join(mediafolder, name))
       print "Temp Folder Cleared..!!"
                                                        
#Delete all the copied Images
def del_images(working_dir,img_names,indexNumb):
    print "Delete"
    print img_names
    for i in range(indexNumb,len(img_names)):
        print img_names[i]
        img_del_path = "%s/%s" %(working_dir,img_names[i])
        os.remove(img_del_path)
        
if __name__ == '__main__':
    # Default set of relationshipships - the minimum components of a document
 
    relationships = relationshiplist()
    print relationships

    # Make a new document tree - this is the main part of a Word document
    document = newdocument()
    body = document.xpath('/w:document/w:body', namespaces=nsprefixes)[0]

    working_dir = os.getcwd()
    #Root Dir Path
    rootDirPath =  os.path.dirname(working_dir)
    #Del Temp Folder files
    del_temp_media(working_dir)
    #Image Dir Path
    img_dir = rootDirPath +'\iPhone_Screen'
    img_names = os.listdir(img_dir)
    print img_names
    change_imgNames = copy_images(working_dir,img_dir,img_names)
    time.sleep(2)
    #
    
    indexNumb = 0
    if(img_names[0] == '.DS_Store'):
           indexNumb = 1
           print "Index is 1"
    else:
           print "indexNumb is ",indexNumb
       
    for i in range (indexNumb,len(change_imgNames)):
        #print img_names[i]
        imgPath = img_dir + "\\" + change_imgNames[i]
        print "IMG",imgPath
        x = change_imgNames[i]
        # Add an image
        body.append(paragraph('/n'))
        relationships, picpara = picture(relationships, x,
                                     'Image description')
        body.append(picpara)
        # Add a pagebreak
        body.append(pagebreak(type='page', orient='portrait'))

    # Create our properties, contenttypes, and other support files
    title    = 'Python docx'
    subject  = 'A practical example of making docx from Python'
    creator  = 'Zaheer'
    keywords = ['python', 'Office Open XML', 'Word']

    coreprops = coreproperties(title=title, subject=subject, creator=creator,
                               keywords=keywords)
    appprops = appproperties()
    contenttypes = contenttypes()
    websettings = websettings()
    wordrelationships = wordrelationships(relationships)

    # Save our document
    savedocx(document, coreprops, appprops, contenttypes, websettings,
             wordrelationships, 'Evid_iPhone_Module.docx')
    print "DOCUMENT CREATED"
    time.sleep(2)
    del_images(working_dir,change_imgNames,indexNumb)
    print "Script OK!"
    
